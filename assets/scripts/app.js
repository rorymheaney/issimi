// for ie
import "babel-polyfill";
// foundation zurb js
import 'foundation-sites';
// owl
// user orbit, if not approved switch to this or something else
// import 'owl.carousel';

// jquery
window.$ = window.jQuery = require("jquery");

// jquery cookie
// import 'js-cookie';
// window.fancySquareCookies = require('js-cookie');

// images loaded
// enque on pages if needed
// masonry
// enque on pages if needed

// axios
window.axios = require('axios');

// pages
import './pages.js'
