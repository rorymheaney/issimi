function fancySquaresMediaElement (argument) {
	$('video, audio').mediaelementplayer({
		// Do not forget to put a final slash (/)
		pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
		// this will allow the CDN to use Flash without restrictions
		// (by default, this is set as `sameDomain`)
		shimScriptAccess: 'always'
		// more configuration
	});
}

function fancySquaresFlexSlider(){
	let $sliderSection = $(".single-layout__slider");

	$(window).load(function() {
		$sliderSection.each(function(){
			// console.log(this)
			let parentSlider = $(this).find('.flexslider:first-child').attr('id'),
				childSlider = $(this).find('.flexslider:last-child').attr('id');
				// console.log(parentSlider);
				// console.log(childSlider);
			$('#' + childSlider).flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav: false,
				itemWidth: 204.5,
				itemMargin: 0,
				minItems: 2,
    			maxItems: 4,
				asNavFor: '#'+parentSlider,
				
			});
	
			$('#'+parentSlider).flexslider({
				animation: "fade",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				sync: '#' + childSlider,
			});
		});
		
		  
	});
}

module.exports = {
	fancySquaresMediaElement: fancySquaresMediaElement,
	fancySquaresFlexSlider: fancySquaresFlexSlider
};
