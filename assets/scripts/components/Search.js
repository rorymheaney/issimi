function issimiSearchResults(){
    
    let $fsLadda = $( '.ladda-button' ).ladda(),
        $loadMoreSearch = $('#load-more-search'),
        activeButtonClass = 'button-type--loading';
    
    $loadMoreSearch.click(function(event){
        event.preventDefault()
        let per_page = $(this).attr('data-posts_per_page'),
            page = $(this).attr('data-pageCount'),
            search = $(this).attr('data-query');

        $fsLadda.ladda( 'start' ).addClass(activeButtonClass);
        $.ajax({
            url: ajaxurl,
            method: "GET",
            dataType: "json",
            data: {
                action: "load_more_search",
                per_page: per_page,
                s: search,
                page: page
            }
        }).done(function(response){
            $fsLadda.ladda( 'stop' ).removeClass(activeButtonClass);
            if(response.data === ""){
                alert("No more posts to show!");
                $loadMoreSearch.remove();
            }
            if(response.status === 'continue'){
                $loadMoreSearch.attr('data-pageCount', Number(page) + 1);
            }else {
                $loadMoreSearch.remove();
            }
            $('#append-content').append(response.data);
        });
    });
    
    
}

module.exports = {
	issimiSearchResults: issimiSearchResults
};
