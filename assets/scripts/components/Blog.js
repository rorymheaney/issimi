function fancySquaresLoadMore (argument) {
	
	console.log('blah');

	let fancySquaresRestUrl = $('body').data('rest'),
		$loadMoreButton = $('#data-parameters'),
		$archiveLayout = $('[data-js="blog"]'),
		loadMoreButtonID = '#data-parameters',
		$fsLadda = $( '.ladda-button' ).ladda(),
		activeButtonClass = 'button-type--loading';

	// start click
	$archiveLayout.on('click',loadMoreButtonID,function(){

	
		let $this = $(this),
			pageCount = $(this).attr('data-pageCount'),
			nextPage = parseInt($(this).attr('data-page')) + 1;

		// var getParams = void 0;
		
		$fsLadda.ladda( 'start' ).addClass(activeButtonClass);
  //       // getParams.pagecount = pageCount;
		// console.log(getParams);

		let form_data = new FormData,
			dataValues = $(this).data();
			
			for (var key in dataValues) {
			    // all_values.push([key, data[key]]);
			    // key[pagecount] = pageCount;
			    if (key === 'pagecount'){

			    	dataValues[key] = Number(pageCount);
			    	form_data.append(key, dataValues[key]);
			    	// console.log(key, data[Number(pageCount)+1]);
			    } else if( key === 'style' || key === 'ladda' || key === 'loading') {
			    	continue;
			    } else {
					form_data.append(key, dataValues[key]);
				}
			    // console.log(key, dataValues[key]);
			}

			// console.log(dataValues);

		axios.post(ajaxurl, form_data)
			.then(function(response){
				// console.log(response);
				// console.log(response.data);

				let postData = response.data;

				if(postData === 'nomore'){
			    	$loadMoreButton.fadeOut();
			    } else {
			    	for(var hp = 0; hp < postData.length; hp++){
						$('[data-js="append-content"]').append(postData[hp].data);
						$this.attr('data-pagecount', Number(pageCount)+1);
						// console.log(postData[hp]);
					}

					
			    }


			    $fsLadda.ladda( 'stop' ).removeClass(activeButtonClass);
			})
			.catch(function (response) {
		        //handle error
		        console.log(response);
		    });

            

	});
	// end click
}


module.exports = {
	fancySquaresLoadMore: fancySquaresLoadMore
};