<?php
/**
 * The main template file
 *
 * @package  WordPress
 * @subpackage  SageTimber
 * @since  SageTimber 0.1
 */

$context = Timber::get_context();
// $context['posts'] = Timber::get_posts();

$firstPosts = TimberHelper::transient('first_posts_transient',
  function(){
	  return $context['posts'] = Timber::get_posts();
	},
  1*HOUR_IN_SECONDS
);

$templates = array( 'pages/index.twig' );
if ( is_home() ) {
	array_unshift( $templates, 'pages/home.twig' );
}

Timber::render( $templates, $context );