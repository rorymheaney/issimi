<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  SageTimber
 * @since  SageTimber 0.1
 */

$templates = array( 'pages/search.twig', 'pages/archive.twig', 'pages/index.twig' );
$context = Timber::get_context();

$context['title'] = 'Search results for '. get_search_query();
$search_posts = new SWP_Query([
    'posts_per_page' => 5,
    's' => get_search_query(),
    //'page' => 1
]);

$ids = [];
if(! empty( $search_posts->posts )){
    foreach($search_posts->posts as $search_post){
        $ids[] = $search_post->ID;
    }
}
$context['posts'] = Timber::get_posts($ids);

//$context['next_page'] = Timber::get_posts('posts_per_page=2&paged=2&s='.get_search_query());
$next_page_posts = new SWP_Query([
    'posts_per_page' => 5,
    's' => get_search_query(),
    'page' => 2
]);

if(! empty( $next_page_posts->posts )) {
    $context['next_page'] = true;
}
else{
    $context['next_page'] = false;
}
$context['search'] = get_search_query();

Timber::render( $templates, $context );