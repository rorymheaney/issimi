<?php 

//
// 
// on upload, automatically add image title and alt
// 
//
add_action( 'add_attachment', 'my_set_image_meta_upon_image_upload' );
function my_set_image_meta_upon_image_upload( $post_ID ) {

  // Check if uploaded file is an image, else do nothing

	if ( wp_attachment_is_image( $post_ID ) ) {

		$my_image_title = get_post( $post_ID )->post_title;

		// Sanitize the title:  remove hyphens, underscores & extra spaces:
		$my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',  $my_image_title );

		// Sanitize the title:  capitalize first letter of every word (other letters lower case):
		$my_image_title = ucwords( strtolower( $my_image_title ) );

		// Create an array with the image meta (Title, Caption, Description) to be updated
		// Note:  comment out the Excerpt/Caption or Content/Description lines if not needed
		$my_image_meta = array(
			'ID'    => $post_ID,      // Specify the image (ID) to be updated
			'post_title'  => $my_image_title,   // Set image Title to sanitized title
			// 'post_excerpt'  => $my_image_title,   // Set image Caption (Excerpt) to sanitized title
			// 'post_content'  => $my_image_title,   // Set image Description (Content) to sanitized title
		);

		// Set the image Alt-Text
		update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );

		// Set the image meta (e.g. Title, Excerpt, Content)
		wp_update_post( $my_image_meta );

	} 
}



// 
// 
// set updated featured image w/ acf image
// 
// 
function acf_set_featured_image( $value, $post_id, $field  ){
	$id = $value;
	if( ! is_numeric( $id ) ){
		$data = json_decode( stripcslashes($id), true );
		$id = $data['cropped_image'];
	}
	update_post_meta( $post_id, '_thumbnail_id', $id );
	return $value;
}

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter( 'acf/update_value/name=fancySquare_featured_img', 'acf_set_featured_image', 10, 3 );


//
// 
// make jpeg quality 100 be default, smush after
// 
//
add_filter('jpeg_quality', function($arg){return 100;});


//
// 
// enque scripts
// 
//
add_action( 'wp_enqueue_scripts', 'fancySquresRosie_enqueue_scripts' );

function fancySquresRosie_enqueue_scripts() {
   

    if(is_single()){
         wp_enqueue_script( 'wp-mediaelement' );
         wp_enqueue_style( 'wp-mediaelement' );
    }

    if(is_front_page || is_search){
        wp_enqueue_style( 'lada-style', get_template_directory_uri() . '/assets/enqueued/ladda.min.css' );
        wp_enqueue_script( 'spin-js', get_template_directory_uri() . '/assets/enqueued/spin.min.js', array("jquery"), '1.0.0', true );
        wp_enqueue_script("lada-js", get_template_directory_uri () . "/assets/enqueued/ladda.min.js",array("jquery"),'1.0.0', true);
    }

    if(is_single){
        wp_enqueue_style( 'flexslider-css', get_template_directory_uri() . '/assets/enqueued/flexslider.css' );
        wp_enqueue_script( 'flexslider-js', get_template_directory_uri() . '/assets/enqueued/jquery.flexslider-min.js', array("jquery"), '1.0.0', true );
    }
}





//
// 
// page views, track those bad boys, we need popular posts!!
// 
//
function fancySquares_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


function fancySquares_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    fancySquares_set_post_views($post_id);
}
add_action( 'wp_head', 'fancySquares_track_post_views');


function fancySquares_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}


//
// 
// add popular posts to context
// 
//
add_filter( 'timber_context', 'fancySquares_show_popular_posts'  );

function fancySquares_show_popular_posts( $context ) {
    $context['fancySquaresPopularPosts'] = Timber::get_posts( fancySquares_find_popular_posts() );
    return $context;
}


function fancySquares_find_popular_posts()
{
    $popularArgs = array(
        'posts_per_page' => 3, 
        'meta_key' => 'wpb_post_views_count', 
        'orderby' => 'meta_value_num', 
        'order' => 'DESC' 
    );

    return $popularArgs;
}



//
// 
// add related posts to context
// 
// 
add_filter( 'timber_context', 'fancySquares_get_related_posts'  );

function fancySquares_get_related_posts( $context ) {
    $context['fancySquaresRelatedPosts'] = Timber::get_posts( fancySquares_set_related_posts() );
    return $context;
}


function fancySquares_set_related_posts()
{
  global $post;

  // Geting the current post's category lists if exists.
  $postcat = get_the_category( $post->ID ); 
  $category_ids = $postcat[0]->term_id;

  // Geting the current post's tags lists if exists.
  $post_tags = wp_get_post_tags($post->ID, array( 'fields' => 'ids' ));

  $related_post = array(
    'post_type' =>'post',
    'category__in' => $category_ids,
    'tag__in' => $post_tags,
    'posts_per_page'=>3,
    'order' => 'DESC',
    'post__not_in' => array($post->ID)
  );

  return $related_post;

}


//
// 
// add seperate CATEGORY query that grabs all the goodies associated (regular and custom meta fields)
// 
//
function add_cat_to_context( $data ){
    $data['cat_id'] = get_query_var('cat');
    // $data['cat_name'] = get_cat_name(get_query_var('cat'));
    return $data;
  }
  add_filter( 'timber_context', 'add_cat_to_context' );