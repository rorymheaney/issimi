<?php

add_action('wp_ajax_more_all_posts', __NAMESPACE__ . '\\ajax_more_all_posts');
add_action('wp_ajax_nopriv_more_all_posts', __NAMESPACE__ . '\\ajax_more_all_posts');
function ajax_more_all_posts()
{ 

    $cat_id = $_REQUEST[ 'cat' ] or $_GET[ 'cat' ];
    $paged = $_REQUEST['pagecount'];
    $cat = $_REQUEST['dataCat'];
    $position = isset($_REQUEST['position']) ? $_REQUEST['position'] : '';
    $author = isset($_REQUEST['author']) ? $_REQUEST['author'] : '';

    $postCount = $_POST['posts_per_page'];

    

    $args = [
        'cat' => $cat_id,
        'posts_per_page' => $postCount,
        'post_status' => 'publish'
    ];

    if($paged != ""){
        $args['paged'] = $paged; 
    }

    $posts = Timber::get_posts($args);
    $message = [];
    if($posts){
        $i = $postCount * ($paged) ? $paged - 2 : 0;
        foreach($posts as $post){
            $response = array(
                'data' => Timber::compile('templates/blocks/content.twig', ['post' => $post, 'position' => $position, 'author' => $author, 'ajaxloop' => $i++])
            );
            $message[] = $response;
        }
    } else {
        $message = "nomore";
    }
    wp_reset_query();
    wp_reset_postdata();

    wp_send_json($message);
    die();
}

add_action('wp_ajax_load_more_search', 'fancySquares_load_more_search');
add_action('wp_ajax_nopriv_load_more_search', 'fancySquares_load_more_search');
function fancySquares_load_more_search(){
    $posts_page_page = $_GET['per_page'];
    $paged = $_GET['page'];
    $query = $_GET['s'];
    $context = Timber::get_context();

    //$context['posts'] = Timber::get_posts("posts_per_page={$posts_page_page}&paged={$paged}&s={$query}");
    $search_posts = new SWP_Query([
        'posts_per_page' => $posts_page_page,
        's' => $query,
        'page' => $paged
    ]);
    $ids = [];
    if(! empty( $search_posts->posts )){
        foreach($search_posts->posts as $search_post){
            $ids[] = $search_post->ID;
        }
    }
    $context['posts'] = Timber::get_posts($ids);

    //$context['next_page'] = Timber::get_posts('posts_per_page=2&paged=2&s='.get_search_query());

    $posts = Timber::compile( 'pages/search_more.twig', $context );
    $next_paged = $paged + 1;
    $next_page_posts = new SWP_Query([
        'posts_per_page' => $posts_page_page,
        's' => $query,
        'page' => $next_paged
    ]);

    //$next_page_posts = Timber::get_posts("posts_per_page={$posts_page_page}&paged={$next_paged}");
    if(! empty( $next_page_posts->posts )){
        $data = [
            'status' => 'continue',
            'data' => $posts
        ];
    } else {
        $data = [
            'status' => 'end',
            'data' => $posts
        ];
    }
    echo json_encode($data);
    die();
}